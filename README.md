
# Quic zig

## Streams

- Streams are an ordered byte-stream abstraction with no other structure visible to QUIC
- STREAM frame boundaries are not expected to be preserved when data is transmitted, retransmitted after packet loss, or delivered to the application at a receiver
- Each stream on a connection
  - has a unique `stream ID`
  - can eather be unidirectional or bidirectional
  - can eather be client or server initiated

- `stream ID` is a 62-bit integer
- `stream ID` is encoded as variable-length integers

| Bits |  Stream Type                      |
| ---- | --------------------------------- |
| 0x00 |  Client-Initiated, Bidirectional  |
| 0x01 |  Server-Initiated, Bidirectional  |
| 0x02 |  Client-Initiated, Unidirectional |
| 0x03 |  Server-Initiated, Unidirectional |

### Operations on Streams

- Sender
  - Write data. respecting stream flow control
  - Cleanly terminate stream. resulting in a STREAM frame (Section 19.8) with the FIN bit set
  - Abruptly terminate stream. resulting in a RESET_STREAM frame

- Receiver
  - Read data
  - Abort reading of the stream and request closure, possibly resulting in a STOP_SENDING frame

- Both ends should be able to request to be informed about all stream state changes
  - When the peer has opened or reset a stream
  - When a peer aborts reading on a stream
  - When new data is available
  - When data can or cannot be written to the stream due to flow control

## Endpoints

- Must be able to deliver data as an ordered byte stream
- Delivering an ordered byte stream requires that an endpoint buffer any data that is received out of order, up to the advertised flow control limit
- An endpoint uses the Stream ID and Offset fields in STREAM frames to place data in order

- The data at a given offset MUST NOT change if it is sent multiple times
- An Endpoint
  - Could receive data at the same offset multiple times. Data that has already been received can be discarded
  - MAY treat receipt of different data at the same offset within a stream as a connection error of type PROTOCOL_VIOLATION
  - MUST NOT send data on any stream without ensuring that it is within the flow control limits set by its peer

- Implementation SHOULD provide ways in which an application can indicate the relative priority of streams & optimize resource allocations accordingly
- Implementations MAY choose to offer the ability to deliver data out of order
